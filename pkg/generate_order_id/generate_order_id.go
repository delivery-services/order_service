package generateorderid

import (
	"fmt"
	"math/rand"
	"time"
)

func GenerateRandomNumber() string {
	rand.Seed(time.Now().UnixNano())
	randomNumber := rand.Intn(999999)
	return fmt.Sprintf("%06d", randomNumber)
}

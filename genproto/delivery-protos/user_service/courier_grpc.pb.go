// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package user_service

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// CourierServiceClient is the client API for CourierService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type CourierServiceClient interface {
	Create(ctx context.Context, in *CreateCourier, opts ...grpc.CallOption) (*Courier, error)
	GetByID(ctx context.Context, in *GetCourierByID, opts ...grpc.CallOption) (*Courier, error)
	GetAll(ctx context.Context, in *GetCourierList, opts ...grpc.CallOption) (*CouriersResponse, error)
	Update(ctx context.Context, in *UpdateCourier, opts ...grpc.CallOption) (*CourierResponse, error)
	Delete(ctx context.Context, in *GetCourierByID, opts ...grpc.CallOption) (*CourierResponse, error)
	UpdatePassword(ctx context.Context, in *UpdateCourierPasswordRequest, opts ...grpc.CallOption) (*CourierResponse, error)
}

type courierServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewCourierServiceClient(cc grpc.ClientConnInterface) CourierServiceClient {
	return &courierServiceClient{cc}
}

func (c *courierServiceClient) Create(ctx context.Context, in *CreateCourier, opts ...grpc.CallOption) (*Courier, error) {
	out := new(Courier)
	err := c.cc.Invoke(ctx, "/user_service.CourierService/Create", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *courierServiceClient) GetByID(ctx context.Context, in *GetCourierByID, opts ...grpc.CallOption) (*Courier, error) {
	out := new(Courier)
	err := c.cc.Invoke(ctx, "/user_service.CourierService/GetByID", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *courierServiceClient) GetAll(ctx context.Context, in *GetCourierList, opts ...grpc.CallOption) (*CouriersResponse, error) {
	out := new(CouriersResponse)
	err := c.cc.Invoke(ctx, "/user_service.CourierService/GetAll", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *courierServiceClient) Update(ctx context.Context, in *UpdateCourier, opts ...grpc.CallOption) (*CourierResponse, error) {
	out := new(CourierResponse)
	err := c.cc.Invoke(ctx, "/user_service.CourierService/Update", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *courierServiceClient) Delete(ctx context.Context, in *GetCourierByID, opts ...grpc.CallOption) (*CourierResponse, error) {
	out := new(CourierResponse)
	err := c.cc.Invoke(ctx, "/user_service.CourierService/Delete", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *courierServiceClient) UpdatePassword(ctx context.Context, in *UpdateCourierPasswordRequest, opts ...grpc.CallOption) (*CourierResponse, error) {
	out := new(CourierResponse)
	err := c.cc.Invoke(ctx, "/user_service.CourierService/UpdatePassword", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// CourierServiceServer is the server API for CourierService service.
// All implementations must embed UnimplementedCourierServiceServer
// for forward compatibility
type CourierServiceServer interface {
	Create(context.Context, *CreateCourier) (*Courier, error)
	GetByID(context.Context, *GetCourierByID) (*Courier, error)
	GetAll(context.Context, *GetCourierList) (*CouriersResponse, error)
	Update(context.Context, *UpdateCourier) (*CourierResponse, error)
	Delete(context.Context, *GetCourierByID) (*CourierResponse, error)
	UpdatePassword(context.Context, *UpdateCourierPasswordRequest) (*CourierResponse, error)
	mustEmbedUnimplementedCourierServiceServer()
}

// UnimplementedCourierServiceServer must be embedded to have forward compatible implementations.
type UnimplementedCourierServiceServer struct {
}

func (UnimplementedCourierServiceServer) Create(context.Context, *CreateCourier) (*Courier, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Create not implemented")
}
func (UnimplementedCourierServiceServer) GetByID(context.Context, *GetCourierByID) (*Courier, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetByID not implemented")
}
func (UnimplementedCourierServiceServer) GetAll(context.Context, *GetCourierList) (*CouriersResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetAll not implemented")
}
func (UnimplementedCourierServiceServer) Update(context.Context, *UpdateCourier) (*CourierResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Update not implemented")
}
func (UnimplementedCourierServiceServer) Delete(context.Context, *GetCourierByID) (*CourierResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Delete not implemented")
}
func (UnimplementedCourierServiceServer) UpdatePassword(context.Context, *UpdateCourierPasswordRequest) (*CourierResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UpdatePassword not implemented")
}
func (UnimplementedCourierServiceServer) mustEmbedUnimplementedCourierServiceServer() {}

// UnsafeCourierServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to CourierServiceServer will
// result in compilation errors.
type UnsafeCourierServiceServer interface {
	mustEmbedUnimplementedCourierServiceServer()
}

func RegisterCourierServiceServer(s grpc.ServiceRegistrar, srv CourierServiceServer) {
	s.RegisterService(&CourierService_ServiceDesc, srv)
}

func _CourierService_Create_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateCourier)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CourierServiceServer).Create(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/user_service.CourierService/Create",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CourierServiceServer).Create(ctx, req.(*CreateCourier))
	}
	return interceptor(ctx, in, info, handler)
}

func _CourierService_GetByID_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetCourierByID)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CourierServiceServer).GetByID(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/user_service.CourierService/GetByID",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CourierServiceServer).GetByID(ctx, req.(*GetCourierByID))
	}
	return interceptor(ctx, in, info, handler)
}

func _CourierService_GetAll_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetCourierList)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CourierServiceServer).GetAll(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/user_service.CourierService/GetAll",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CourierServiceServer).GetAll(ctx, req.(*GetCourierList))
	}
	return interceptor(ctx, in, info, handler)
}

func _CourierService_Update_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdateCourier)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CourierServiceServer).Update(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/user_service.CourierService/Update",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CourierServiceServer).Update(ctx, req.(*UpdateCourier))
	}
	return interceptor(ctx, in, info, handler)
}

func _CourierService_Delete_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetCourierByID)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CourierServiceServer).Delete(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/user_service.CourierService/Delete",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CourierServiceServer).Delete(ctx, req.(*GetCourierByID))
	}
	return interceptor(ctx, in, info, handler)
}

func _CourierService_UpdatePassword_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdateCourierPasswordRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CourierServiceServer).UpdatePassword(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/user_service.CourierService/UpdatePassword",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CourierServiceServer).UpdatePassword(ctx, req.(*UpdateCourierPasswordRequest))
	}
	return interceptor(ctx, in, info, handler)
}

// CourierService_ServiceDesc is the grpc.ServiceDesc for CourierService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var CourierService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "user_service.CourierService",
	HandlerType: (*CourierServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Create",
			Handler:    _CourierService_Create_Handler,
		},
		{
			MethodName: "GetByID",
			Handler:    _CourierService_GetByID_Handler,
		},
		{
			MethodName: "GetAll",
			Handler:    _CourierService_GetAll_Handler,
		},
		{
			MethodName: "Update",
			Handler:    _CourierService_Update_Handler,
		},
		{
			MethodName: "Delete",
			Handler:    _CourierService_Delete_Handler,
		},
		{
			MethodName: "UpdatePassword",
			Handler:    _CourierService_UpdatePassword_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "delivery-protos/user_service/courier.proto",
}

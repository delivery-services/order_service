package grpc

import (
	pbo "order_service/genproto/delivery-protos/order_service"
	"order_service/grpc/client"
	"order_service/service"
	"order_service/storage"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(storage storage.IStorage, services client.IServiceManager) *grpc.Server {
	grpcServer := grpc.NewServer()

	pbo.RegisterOrderServiceServer(grpcServer, service.NewOrderService(storage, services))
	pbo.RegisterOrderProductServiceServer(grpcServer, service.NewOrderProductService(storage, services))
	pbo.RegisterAlternativeServiceServer(grpcServer, service.NewAlternativeService(storage, services))
	pbo.RegisterDeliveryTarifServicesServer(grpcServer, service.NewDeliveryTariffService(storage, services))
	

	reflection.Register(grpcServer)

	return grpcServer

}

package client

import (
	"order_service/config"
	"order_service/genproto/delivery-protos/order_service"
	"order_service/genproto/delivery-protos/user_service"

	"google.golang.org/grpc"
)

type IServiceManager interface {
	OrderService() order_service.OrderServiceClient
	OrderProductService() order_service.OrderProductServiceClient
	AlternativeService() order_service.AlternativeServiceClient
	DeliveryTarifService() order_service.DeliveryTarifServicesClient
	BranchService() user_service.BranchServiceClient
	ClientService() user_service.ClientServiceClient
}

type grpcClients struct {
	orderService         order_service.OrderServiceClient
	orderProductService  order_service.OrderProductServiceClient
	alternativeService   order_service.AlternativeServiceClient
	deliveryTarifService order_service.DeliveryTarifServicesClient
	branchService        user_service.BranchServiceClient
	clientService        user_service.ClientServiceClient
}

func NewGrpcClients(cfg config.Config) (IServiceManager, error) {
	connOrderService, err := grpc.Dial(
		cfg.ServiceGrpcHost+cfg.ServiceGrpcPort,
		grpc.WithInsecure(),
	)
	if err != nil {
		return nil, err
	}

	connUserService, err := grpc.Dial(
		cfg.UserServiceGrpcHost+cfg.UserServiceGrpcPort,
		grpc.WithInsecure(),
	)
	if err != nil {
		return nil, err
	}

	return &grpcClients{
		orderService:         order_service.NewOrderServiceClient(connOrderService),
		orderProductService:  order_service.NewOrderProductServiceClient(connOrderService),
		alternativeService:   order_service.NewAlternativeServiceClient(connOrderService),
		deliveryTarifService: order_service.NewDeliveryTarifServicesClient(connOrderService),
		branchService:        user_service.NewBranchServiceClient(connUserService),
		clientService:        user_service.NewClientServiceClient(connUserService),
	}, nil
}

func (g *grpcClients) OrderService() order_service.OrderServiceClient {
	return g.orderService
}

func (g *grpcClients) OrderProductService() order_service.OrderProductServiceClient {
	return g.orderProductService
}

func (g *grpcClients) AlternativeService() order_service.AlternativeServiceClient {
	return g.alternativeService
}

func (g *grpcClients) DeliveryTarifService() order_service.DeliveryTarifServicesClient {
	return g.deliveryTarifService
}

func (g *grpcClients) BranchService() user_service.BranchServiceClient {
	return g.branchService
}

func (g *grpcClients) ClientService() user_service.ClientServiceClient {
	return g.clientService
}

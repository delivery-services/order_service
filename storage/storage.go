package storage

import (
	"context"
	pb "order_service/genproto/delivery-protos/order_service"
)

type IStorage interface {
	Close()
	OrderProduct() IOrderProductStorage
	Alternative() IAlternativeStorage
	DeliveryTariff() IDeliveryTariffStorage
	Order() IOrderStorage
}

type IOrderProductStorage interface {
	Create(context.Context, *pb.CreateOrderProduct) (*pb.OrderProduct, error)
	GetByID(context.Context, *pb.GetOrderProductByID) (*pb.OrderProduct, error)
	GetAll(context.Context, *pb.GetOrderProductList) (*pb.OrderProductsResponse, error)
	Update(context.Context, *pb.UpdateOrderProduct) (*pb.OrderProductResponse, error)
	Delete(context.Context, *pb.GetOrderProductByID) (*pb.OrderProductResponse, error)
}

type IAlternativeStorage interface {
	Create(context.Context, *pb.CreateAlternative) (*pb.Alternative, error)
	GetByID(context.Context, *pb.GetAlternativeByID) (*pb.Alternative, error)
	GetAll(context.Context, *pb.GetAlternativeList) (*pb.AlternativesResponse, error)
	Update(context.Context, *pb.UpdateAlternative) (*pb.AlternativeResponse, error)
	Delete(context.Context, *pb.GetAlternativeByID) (*pb.AlternativeResponse, error)
}

type IDeliveryTariffStorage interface {
	Create(context.Context, *pb.CreateDeliveryTarif) (*pb.DeliveryTarif, error)
	GetByID(context.Context, *pb.GetDeliveryTarifByID) (*pb.DeliveryTarif, error)
	GetAll(context.Context, *pb.GetDeliveryTarifList) (*pb.DeliveryTarifsResponse, error)
	Update(context.Context, *pb.UpdateDeliveryTarif) (*pb.DeliveryTarifResponse, error)
	Delete(context.Context, *pb.GetDeliveryTarifByID) (*pb.DeliveryTarifResponse, error)
}

type IOrderStorage interface {
	Create(context.Context, *pb.CreateOrder) (*pb.Order, error)
	GetByID(context.Context, *pb.GetOrderByID) (*pb.Order, error)
	GetAll(context.Context, *pb.GetOrderList) (*pb.OrdersResponse, error)
	Update(context.Context, *pb.UpdateOrder) (*pb.OrderResponse, error)
	Delete(context.Context, *pb.GetOrderByID) (*pb.OrderResponse, error)
	UpdateOrderStatus(context.Context, *pb.Order) (*pb.OrderResponse, error)
}

package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	pbo "order_service/genproto/delivery-protos/order_service"
	"order_service/pkg/logger"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
)

type orderProductRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
	pbo.UnimplementedOrderProductServiceServer
}

func NewOrderProductRepo(db *pgxpool.Pool, log logger.ILogger) *orderProductRepo {
	return &orderProductRepo{
		db:  db,
		log: log,
	}
}

func (o *orderProductRepo) Create(ctx context.Context, orderProduct *pbo.CreateOrderProduct) (*pbo.OrderProduct, error) {
	var updatedAt = sql.NullString{}

	var response = pbo.OrderProduct{}

	id := uuid.New()

	query := `INSERT INTO order_product (
		id, 
		product_id,
		quantity,
		price
	) VALUES ($1, $2, $3, $4)
	   RETURNING
		id, 
		product_id,
		quantity,
		price, 
		created_at::text, 
		updated_at::text
	`

	if err := o.db.QueryRow(ctx, query,
		id,
		orderProduct.ProductId,
		orderProduct.Quantity,
		orderProduct.Price,
	).Scan(
		&response.Id,
		&response.ProductId,
		&response.Quantity,
		&response.Price,
		&response.CreatedAt,
		&updatedAt,
	); err != nil {
		o.log.Error("error in order product service while inserting and getting order product")
		return nil, err
	}

	if updatedAt.Valid {
		response.UpdatedAt = updatedAt.String
	}

	return &response, nil
}

func (o *orderProductRepo) GetByID(ctx context.Context, pKey *pbo.GetOrderProductByID) (*pbo.OrderProduct, error) {
	var updatedAt = sql.NullString{}
	orderProduct := &pbo.OrderProduct{}

	query := `SELECT 
		id, 
		product_id,
		quantity,
		price,
		created_at::text, 
		updated_at::text 
	FROM order_product 
	WHERE deleted_at = 0 AND id = $1`

	row := o.db.QueryRow(ctx, query, pKey.Id)

	err := row.Scan(
		&orderProduct.Id,
		&orderProduct.ProductId,
		&orderProduct.Quantity,
		&orderProduct.Price,
		&orderProduct.CreatedAt,
		&updatedAt,
	)

	if err != nil {
		log.Println("error while selecting order product", err.Error())
		return &pbo.OrderProduct{}, err
	}

	if updatedAt.Valid {
		orderProduct.UpdatedAt = updatedAt.String
	}

	return orderProduct, nil
}

func (o *orderProductRepo) GetAll(ctx context.Context, request *pbo.GetOrderProductList) (*pbo.OrderProductsResponse, error) {
	var (
		orderProducts     = []*pbo.OrderProduct{}
		count             = 0
		query, countQuery string
		page              = request.Page
		offset            = (page - 1) * request.Limit
		search            = request.Search
		updatedAt         = sql.NullString{}
	)

	countQuery = `SELECT COUNT(1) FROM order_product WHERE deleted_at = 0 `

	if search != "" {
		countQuery += fmt.Sprintf(` AND (quantity::text ILIKE '%%%s%%' OR price::text ILIKE '%%%s%%')`, search, search)
	}

	if err := o.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		o.log.Error("error while selecting order product count", logger.Error(err))
		return &pbo.OrderProductsResponse{}, err
	}

	query = `SELECT 
		id, 
		product_id,
		quantity,
		price,
		created_at::text, 
		updated_at::text 
	FROM order_product 
	WHERE deleted_at = 0 `

	if search != "" {
		query += fmt.Sprintf(` AND (quantity::text ILIKE '%%%s%%' OR price::text ILIKE '%%%s%%')`, search, search)
	}

	query += ` ORDER BY created_at DESC LIMIT $1 OFFSET $2`

	rows, err := o.db.Query(ctx, query, request.Limit, offset)
	if err != nil {
		o.log.Error("error while selecting order product", logger.Error(err))
		return &pbo.OrderProductsResponse{}, err
	}

	defer rows.Close()

	for rows.Next() {
		orderProduct := pbo.OrderProduct{}
		if err = rows.Scan(
			&orderProduct.Id,
			&orderProduct.ProductId,
			&orderProduct.Quantity,
			&orderProduct.Price,
			&orderProduct.CreatedAt,
			&updatedAt,
		); err != nil {
			o.log.Error("error while scanning order product data", logger.Error(err))
			return &pbo.OrderProductsResponse{}, err
		}

		if updatedAt.Valid {
			orderProduct.UpdatedAt = updatedAt.String
		}

		orderProducts = append(orderProducts, &orderProduct)
	}

	if err := rows.Err(); err != nil {
		o.log.Error("error while iterating rows", logger.Error(err))
		return &pbo.OrderProductsResponse{}, err
	}

	return &pbo.OrderProductsResponse{
		OrderProduct: orderProducts,
		Count:        int32(count),
	}, nil
}

func (o *orderProductRepo) Update(ctx context.Context, request *pbo.UpdateOrderProduct) (*pbo.OrderProductResponse, error) {
	query := `
		UPDATE order_product SET 
		product_id = $1, 
		quantity = $2,
        price = $3,
		updated_at = NOW()
		WHERE id = $4`

	if _, err := o.db.Exec(ctx, query,
		request.ProductId,
		request.Quantity,
		request.Price,
		request.Id,
	); err != nil {
		fmt.Println("error while updating order_product data", err.Error())
		return &pbo.OrderProductResponse{}, err
	}

	return &pbo.OrderProductResponse{
		Response: "data successfully updated",
	}, nil
}

func (o *orderProductRepo) Delete(ctx context.Context, request *pbo.GetOrderProductByID) (*pbo.OrderProductResponse, error) {

	query := `UPDATE order_product SET deleted_at = EXTRACT(EPOCH FROM CURRENT_TIMESTAMP) WHERE id = $1`

	if _, err := o.db.Exec(ctx, query, request.Id); err != nil {
		fmt.Println("error while deleting order product by id", err.Error())
		return nil, err
	}

	return &pbo.OrderProductResponse{
		Response: "data successfully deleted",
	}, nil
}

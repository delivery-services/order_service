package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	pbo "order_service/genproto/delivery-protos/order_service"
	"order_service/pkg/logger"
	"time"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
)

type orderRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
	pbo.UnimplementedOrderServiceServer
}

func NewOrderRepo(db *pgxpool.Pool, log logger.ILogger) *orderRepo {
	return &orderRepo{
		db:  db,
		log: log,
	}
}

func (o *orderRepo) Create(ctx context.Context, order *pbo.CreateOrder) (*pbo.Order, error) {

	var branchName string
	err := o.db.QueryRow(ctx, "SELECT name FROM branch WHERE id = $1", order.BranchId).Scan(&branchName)
	if err != nil {
		o.log.Error("error retrieving branch name:", logger.Error(err))
		return nil, err
	}

	branchPrefix := branchName + "_"
	countQuery := `SELECT COUNT(1) FROM orders WHERE branch_id = $1`

	var count int
	if err := o.db.QueryRow(ctx, countQuery, order.BranchId).Scan(&count); err != nil {
		o.log.Error("error getting order count for branch:", logger.Error(err))
		return nil, err
	}

	orderNumber := fmt.Sprintf("%s%06d", branchPrefix, count+1)
	query := `INSERT INTO orders (
        id, 
        client_id,
        branch_id,
        type,
        address,
        courier_id,
        price,
        delivery_price,
        discount,
        status,
        payment_type,
        order_product_id,
		order_number
    ) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13)
       RETURNING
        id, 
        client_id,
        branch_id,
        type,
        address,
        courier_id,
        price,
        delivery_price,
        discount,
        status,
        payment_type,
        order_product_id, 
		order_number,
		created_at::text, 
		updated_at::text
    `


	var response pbo.Order
	var updatedAt sql.NullString
	if err := o.db.QueryRow(ctx, query,
		uuid.New().String(),
		order.ClientId,
		order.BranchId,
		order.Type,
		order.Address,
		order.CourierId,
		order.Price,
		order.DeliveryPrice,
		order.Discount,
		order.Status,
		order.PaymentType,
		order.OrderProductId,
		orderNumber,
	).Scan(
		&response.Id,
		&response.ClientId,
		&response.BranchId,
		&response.Type,
		&response.Address,
		&response.CourierId,
		&response.Price,
		&response.DeliveryPrice,
		&response.Discount,
		&response.Status,
		&response.PaymentType,
		&response.OrderProductId,
		&response.OrderId,
		&response.CreatedAt,
		&updatedAt,
	); err != nil {
		o.log.Error("error in order service while inserting and getting order")
		return nil, err
	}

	if updatedAt.Valid {
		response.UpdatedAt = updatedAt.String
	}

	return &response, nil
}

func (o *orderRepo) GetByID(ctx context.Context, pKey *pbo.GetOrderByID) (*pbo.Order, error) {
	var updatedAt = sql.NullString{}
	order := &pbo.Order{}

	query := `SELECT 
        id, 
        order_number,
        client_id,
        branch_id,
        type,
        address,
        courier_id,
        price,
        delivery_price,
        discount,
        status,
        payment_type,
        order_product_id,
        created_at::text, 
        updated_at::text 
    FROM orders 
    WHERE deleted_at = 0 AND id = $1`

	row := o.db.QueryRow(ctx, query, pKey.Id)

	err := row.Scan(
		&order.Id,
		&order.OrderId,
		&order.ClientId,
		&order.BranchId,
		&order.Type,
		&order.Address,
		&order.CourierId,
		&order.Price,
		&order.DeliveryPrice,
		&order.Discount,
		&order.Status,
		&order.PaymentType,
		&order.OrderProductId,
		&order.CreatedAt,
		&updatedAt,
	)

	if err != nil {
		log.Println("error while selecting order", err.Error())
		return &pbo.Order{}, err
	}

	if updatedAt.Valid {
		order.UpdatedAt = updatedAt.String
	}

	return order, nil
}

func (o *orderRepo) GetAll(ctx context.Context, request *pbo.GetOrderList) (*pbo.OrdersResponse, error) {
	var (
		orders            = []*pbo.Order{}
		count             = 0
		query, countQuery string
		page              = request.Page
		offset            = (page - 1) * request.Limit
		search            = request.Search
		updatedAt         = sql.NullString{}
	)

	countQuery = `SELECT COUNT(1) FROM orders WHERE deleted_at = 0 `

	if search != "" {
		countQuery += fmt.Sprintf(` AND (price::text ILIKE '%%%s%%' OR status::text ILIKE '%%%s%%')`, search, search)
	}

	if err := o.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		o.log.Error("error while selecting order count", logger.Error(err))
		return &pbo.OrdersResponse{}, err
	}

	query = `SELECT 
		id, 
		order_number,
		client_id,
		branch_id,
		type,
		address,
		courier_id,
		price,
		delivery_price,
        discount,
		status,
		payment_type,
		order_product_id,
		created_at::text, 
		updated_at::text 
	FROM orders 
	WHERE deleted_at = 0 `

	if search != "" {
		query += fmt.Sprintf(` AND (price::text ILIKE '%%%s%%' OR status::text ILIKE '%%%s%%')`, search, search)
	}

	query += ` ORDER BY created_at DESC LIMIT $1 OFFSET $2`

	rows, err := o.db.Query(ctx, query, request.Limit, offset)
	if err != nil {
		o.log.Error("error while selecting order", logger.Error(err))
		return &pbo.OrdersResponse{}, err
	}

	defer rows.Close()

	for rows.Next() {
		order := pbo.Order{}
		if err = rows.Scan(
			&order.Id,
			&order.OrderId,
			&order.ClientId,
			&order.BranchId,
			&order.Type,
			&order.Address,
			&order.CourierId,
			&order.Price,
			&order.DeliveryPrice,
			&order.Discount,
			&order.Status,
			&order.PaymentType,
			&order.OrderProductId,
			&order.CreatedAt,
			&updatedAt,
		); err != nil {
			o.log.Error("error while scanning order data", logger.Error(err))
			return &pbo.OrdersResponse{}, err
		}

		if updatedAt.Valid {
			order.UpdatedAt = updatedAt.String
		}

		orders = append(orders, &order)
	}

	if err := rows.Err(); err != nil {
		o.log.Error("error while iterating rows", logger.Error(err))
		return &pbo.OrdersResponse{}, err
	}

	return &pbo.OrdersResponse{
		Order: orders,
		Count: int32(count),
	}, nil
}

func (o *orderRepo) Update(ctx context.Context, request *pbo.UpdateOrder) (*pbo.OrderResponse, error) {
	query := `
		UPDATE orders SET 
		order_number = $1, 
		client_id = $2,
        branch_id = $3,
		type = $4,
		address = $5,
		courier_id = $6,
		price = $7,
		delivery_price = $8,
		discount = $9,
		status = $10,
		payment_type = $11,
		order_product_id = $12,
		updated_at = $13
		WHERE id = $14`

	if _, err := o.db.Exec(ctx, query,
		request.OrderId,
		request.ClientId,
		request.BranchId,
		request.Type,
		request.Address,
		request.CourierId,
		request.Price,
		request.DeliveryPrice,
		request.Discount,
		request.Status,
		request.PaymentType,
		request.OrderProductId,
		time.Now(),
		request.Id,
	); err != nil {
		fmt.Println("error while updating order data", err.Error())
		return &pbo.OrderResponse{}, err
	}

	return &pbo.OrderResponse{
		Response: "data successfully updated",
	}, nil
}

func (o *orderRepo) Delete(ctx context.Context, request *pbo.GetOrderByID) (*pbo.OrderResponse, error) {

	query := `UPDATE orders SET deleted_at = EXTRACT(EPOCH FROM CURRENT_TIMESTAMP) WHERE id = $1`

	if _, err := o.db.Exec(ctx, query, request.Id); err != nil {
		fmt.Println("error while deleting order  by id", err.Error())
		return nil, err
	}

	return &pbo.OrderResponse{
		Response: "data successfully deleted",
	}, nil
}

func (o *orderRepo) UpdateOrderStatus(ctx context.Context, request *pbo.Order) (*pbo.OrderResponse, error) {

	query := `UPDATE orders set status = $1, updated_at = $2 where id = $3`

	if _, err := o.db.Exec(ctx, query, request.Status, time.Now(), request.Id); err != nil {
		fmt.Println("error while updating order status", err.Error())
		return nil, err
	}

	return &pbo.OrderResponse{
		Response: "status succesfully updated",
	}, nil

}

package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"order_service/genproto/delivery-protos/order_service"
	"order_service/pkg/logger"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
)

type alternativeRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
	order_service.UnimplementedAlternativeServiceServer
}

func NewAlternativeRepo(db *pgxpool.Pool, log logger.ILogger) *alternativeRepo {
	return &alternativeRepo{
		db:  db,
		log: log,
	}
}

func (a *alternativeRepo) Create(ctx context.Context, alt *order_service.CreateAlternative) (*order_service.Alternative, error) {
	var response = order_service.Alternative{}
	var updatedAt = sql.NullString{}

	id := uuid.New()

	query := `INSERT INTO alternative (
		id, 
		from_price,
		to_price,
		price
	) VALUES ($1, $2, $3, $4)
	   RETURNING
		id, 
		from_price,
		to_price,
		price,
		created_at::text, 
		updated_at::text 
	`

	if err := a.db.QueryRow(ctx, query,
		id,
		alt.FromPrice,
		alt.ToPrice,
		alt.Price,
	).Scan(
		&response.Id,
		&response.FromPrice,
		&response.ToPrice,
		&response.Price,
		&response.CreatedAt,
		&updatedAt,
	); err != nil {
		a.log.Error("error in alternative service while inserting and getting alternative", logger.Error(err))
		return nil, err
	}

	if updatedAt.Valid {
		response.UpdatedAt = updatedAt.String
	}

	return &response, nil
}

func (a *alternativeRepo) GetByID(ctx context.Context, pKey *order_service.GetAlternativeByID) (*order_service.Alternative, error) {
	var updatedAt = sql.NullString{}
	alternative := &order_service.Alternative{}

	query := `SELECT 
		id, 
		from_price,
		to_price,
		price,
		created_at::text, 
		updated_at::text 
	FROM alternative 
	WHERE deleted_at = 0 AND id = $1`

	row := a.db.QueryRow(ctx, query, pKey.Id)

	err := row.Scan(
		&alternative.Id,
		&alternative.FromPrice,
		&alternative.ToPrice,
		&alternative.Price,
		&alternative.CreatedAt,
		&updatedAt,
	)

	if err != nil {
		a.log.Error("error while selecting alternative", logger.Error(err))
		return &order_service.Alternative{}, err
	}

	if updatedAt.Valid {
		alternative.UpdatedAt = updatedAt.String
	}

	return alternative, nil
}

func (a *alternativeRepo) GetAll(ctx context.Context, request *order_service.GetAlternativeList) (*order_service.AlternativesResponse, error) {
	var (
		alternatives      = []*order_service.Alternative{}
		count             = 0
		query, countQuery string
		page              = request.Page
		offset            = (page - 1) * request.Limit
		search            = request.Search
		updatedAt         = sql.NullString{}
	)

	countQuery = `SELECT COUNT(1) FROM alternative WHERE deleted_at = 0 `

	if search != "" {
		countQuery += fmt.Sprintf(` AND (from_price::text ILIKE '%%%s%%' OR to_price::text ILIKE '%%%s%%' OR price::text ILIKE '%%%s%%')`, search, search, search)
	}

	if err := a.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		a.log.Error("error while selecting alternative count", logger.Error(err))
		return &order_service.AlternativesResponse{}, err
	}

	query = `SELECT 
		id, 
		from_price,
		to_price,
		price,
		created_at::text, 
		updated_at::text 
	FROM alternative 
	WHERE deleted_at = 0 `

	if search != "" {
		query += fmt.Sprintf(` AND (from_price::text ILIKE '%%%s%%' OR to_price::text ILIKE '%%%s%%' OR price::text ILIKE '%%%s%%')`, search, search, search)
	}

	query += ` ORDER BY created_at DESC LIMIT $1 OFFSET $2`

	rows, err := a.db.Query(ctx, query, request.Limit, offset)
	if err != nil {
		a.log.Error("error while selecting alternative", logger.Error(err))
		return &order_service.AlternativesResponse{}, err
	}

	defer rows.Close()

	for rows.Next() {
		alternative := order_service.Alternative{}
		if err = rows.Scan(
			&alternative.Id,
			&alternative.FromPrice,
			&alternative.ToPrice,
			&alternative.Price,
			&alternative.CreatedAt,
			&updatedAt,
		); err != nil {
			a.log.Error("error while scanning alternative data", logger.Error(err))
			return &order_service.AlternativesResponse{}, err
		}

		if updatedAt.Valid {
			alternative.UpdatedAt = updatedAt.String
		}

		alternatives = append(alternatives, &alternative)
	}

	if err := rows.Err(); err != nil {
		a.log.Error("error while iterating rows", logger.Error(err))
		return &order_service.AlternativesResponse{}, err
	}

	return &order_service.AlternativesResponse{
		Alternative: alternatives,
		Count:       int32(count),
	}, nil
}

func (a *alternativeRepo) Update(ctx context.Context, request *order_service.UpdateAlternative) (*order_service.AlternativeResponse, error) {
	query := `
		UPDATE alternative SET 
		from_price = $1, 
		to_price = $2,
        price = $3,
		updated_at = NOW()
		WHERE id = $4`

	if _, err := a.db.Exec(ctx, query,
		request.FromPrice,
		request.ToPrice,
		request.Price,
		request.Id,
	); err != nil {
		fmt.Println("error while updating alternative data", err.Error())
		return &order_service.AlternativeResponse{}, err
	}

	return &order_service.AlternativeResponse{
		Response: "data successfully updated",
	}, nil
}

func (a *alternativeRepo) Delete(ctx context.Context, request *order_service.GetAlternativeByID) (*order_service.AlternativeResponse, error) {
	query := `UPDATE alternative SET deleted_at = EXTRACT(EPOCH FROM CURRENT_TIMESTAMP) WHERE id = $1`

	if _, err := a.db.Exec(ctx, query, request.Id); err != nil {
		fmt.Println("error while deleting alternative by id", err.Error())
		return nil, err
	}

	return &order_service.AlternativeResponse{
		Response: "data successfully deleted",
	}, nil
}

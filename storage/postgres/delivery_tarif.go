package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"order_service/genproto/delivery-protos/order_service"
	"order_service/pkg/logger"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
)

type deliveryTarifRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
	order_service.UnimplementedDeliveryTarifServicesServer
}

func NewDeliveryTariffRepo(db *pgxpool.Pool, log logger.ILogger) *deliveryTarifRepo {
	return &deliveryTarifRepo{
		db:  db,
		log: log,
	}
}

func (d *deliveryTarifRepo) Create(ctx context.Context, tarif *order_service.CreateDeliveryTarif) (*order_service.DeliveryTarif, error) {
	var updatedAt = sql.NullString{}

	id := uuid.New()

	query := `
        INSERT INTO delivery_tarif (
            id,
            name,
            type,
            base_price,
            alternative_id
        ) VALUES ($1, $2, $3, $4, $5)
        RETURNING 
		id, 
		name, 
		type, 
		base_price, 
		alternative_id, 
		created_at::text, 
		updated_at::text
    `
	row := d.db.QueryRow(ctx, query,
		id,
		tarif.Name,
		tarif.Type,
		tarif.BasePrice,
		tarif.AlternativeId,
	)

	createdTarif := &order_service.DeliveryTarif{}
	if err := row.Scan(
		&createdTarif.Id,
		&createdTarif.Name,
		&createdTarif.Type,
		&createdTarif.BasePrice,
		&createdTarif.AlternativeId,
		&createdTarif.CreatedAt,
		&updatedAt,
	); err != nil {
		d.log.Error("error while creating delivery tarif", logger.Error(err))
		return nil, err
	}

	if updatedAt.Valid {
		createdTarif.UpdatedAt = updatedAt.String
	}

	return createdTarif, nil
}

func (d *deliveryTarifRepo) GetByID(ctx context.Context, pKey *order_service.GetDeliveryTarifByID) (*order_service.DeliveryTarif, error) {

	var updatedAt = sql.NullString{}

	query := `
        SELECT
            id,
            name,
            type,
            base_price,
            alternative_id,
            created_at::text,
            updated_at::text
        FROM
            delivery_tarif
        WHERE
            deleted_at = 0
            AND id = $1
    `

	row := d.db.QueryRow(ctx, query, pKey.Id)

	tarif := &order_service.DeliveryTarif{}
	if err := row.Scan(
		&tarif.Id,
		&tarif.Name,
		&tarif.Type,
		&tarif.BasePrice,
		&tarif.AlternativeId,
		&tarif.CreatedAt,
		&updatedAt,
	); err != nil {
		d.log.Error("error while getting delivery tarif by ID", logger.Error(err))
		return nil, err
	}

	if updatedAt.Valid {
		tarif.UpdatedAt = updatedAt.String
	}

	return tarif, nil
}

func (d *deliveryTarifRepo) GetAll(ctx context.Context, request *order_service.GetDeliveryTarifList) (*order_service.DeliveryTarifsResponse, error) {
	var (
		tarifs            = []*order_service.DeliveryTarif{}
		count             = 0
		query, countQuery string
		page              = request.Page
		limit             = request.Limit
		offset            = (page - 1) * limit
		search            = request.Search
		updatedAt         = sql.NullString{}
	)

	countQuery = `SELECT COUNT(1) FROM delivery_tarif WHERE deleted_at = 0`

	if search != "" {
		countQuery += fmt.Sprintf(` AND (name ILIKE '%%%s%%' OR type ILIKE '%%%s%%')`, search, search)
	}

	if err := d.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		d.log.Error("error while getting delivery tarif count", logger.Error(err))
		return &order_service.DeliveryTarifsResponse{}, err
	}

	query = `
        SELECT
            id,
            name,
            type,
            base_price,
            alternative_id,
            created_at::text,
            updated_at::text
        FROM
            delivery_tarif
        WHERE
            deleted_at = 0
    `

	if search != "" {
		query += fmt.Sprintf(` AND (name ILIKE '%%%s%%' OR type ILIKE '%%%s%%')`, search, search)
	}

	rows, err := d.db.Query(ctx, query+` ORDER BY created_at DESC LIMIT $1 OFFSET $2`, limit, offset)
	if err != nil {
		d.log.Error("error while getting delivery tarifs", logger.Error(err))
		return &order_service.DeliveryTarifsResponse{}, err
	}

	defer rows.Close()

	for rows.Next() {
		tarif := &order_service.DeliveryTarif{}
		if err := rows.Scan(
			&tarif.Id,
			&tarif.Name,
			&tarif.Type,
			&tarif.BasePrice,
			&tarif.AlternativeId,
			&tarif.CreatedAt,
			&updatedAt,
		); err != nil {
			d.log.Error("error while scanning delivery tarif data", logger.Error(err))
			return &order_service.DeliveryTarifsResponse{}, err
		}

		if updatedAt.Valid {
			tarif.UpdatedAt = updatedAt.String
		}

		tarifs = append(tarifs, tarif)
	}

	if err := rows.Err(); err != nil {
		d.log.Error("error while iterating rows", logger.Error(err))
		return &order_service.DeliveryTarifsResponse{}, err
	}

	return &order_service.DeliveryTarifsResponse{
		DeliveryTarif: tarifs,
		Count:         int32(count),
	}, nil
}

func (d *deliveryTarifRepo) Update(ctx context.Context, request *order_service.UpdateDeliveryTarif) (*order_service.DeliveryTarifResponse, error) {

	query := `
        UPDATE delivery_tarif SET
            name = $1,
            type = $2,
            base_price = $3,
            alternative_id = $4,
            updated_at = NOW()
        WHERE id = $5
    `
	if _, err := d.db.Exec(ctx, query,
		request.Name,
		request.Type,
		request.BasePrice,
		request.AlternativeId,
		request.Id,
	); err != nil {
		d.log.Error("error while updating delivery tarif", logger.Error(err))
		return &order_service.DeliveryTarifResponse{}, err
	}

	return &order_service.DeliveryTarifResponse{
		Response: "data successfully updated",
	}, nil
}

func (d *deliveryTarifRepo) Delete(ctx context.Context, request *order_service.GetDeliveryTarifByID) (*order_service.DeliveryTarifResponse, error) {

	query := `
        UPDATE delivery_tarif SET
            deleted_at = EXTRACT(EPOCH FROM CURRENT_TIMESTAMP)
        WHERE id = $1
    `

	if _, err := d.db.Exec(ctx, query, request.Id); err != nil {
		d.log.Error("error while deleting delivery tarif by ID", logger.Error(err))
		return nil, err
	}

	return &order_service.DeliveryTarifResponse{
		Response: "data successfully deleted",
	}, nil

}

package service

import (
	"context"
	"fmt"
	"log"
	pbo "order_service/genproto/delivery-protos/order_service"
	"order_service/grpc/client"
	"order_service/storage"
)

type orderProductService struct {
	storage  storage.IStorage
	services client.IServiceManager
	pbo.UnimplementedOrderProductServiceServer
}

func NewOrderProductService(storage storage.IStorage, services client.IServiceManager) *orderProductService {
	return &orderProductService{
		storage:  storage,
		services: services,
	}
}

func (o *orderProductService) Create(ctx context.Context, request *pbo.CreateOrderProduct) (*pbo.OrderProduct, error) {
	client, err := o.storage.OrderProduct().Create(ctx, request)
	if err != nil {
		log.Println("error in order product service in service package while creating order product")
		return nil, err
	}
	return client, nil
}

func (o *orderProductService) GetByID(ctx context.Context, request *pbo.GetOrderProductByID) (*pbo.OrderProduct, error) {
	client, err := o.storage.OrderProduct().GetByID(ctx, request)
	if err != nil {
		fmt.Println("error in service layer while getting order product by id")
		return nil, err
	}
	return client, nil
}

func (o *orderProductService) GetAll(ctx context.Context, request *pbo.GetOrderProductList) (*pbo.OrderProductsResponse, error) {

	orderProducts, err := o.storage.OrderProduct().GetAll(ctx, request)

	if err != nil {

		fmt.Println("error in service layer while getting order product list")

		return nil, err

	}
	return orderProducts, nil
}

func (o *orderProductService) Update(ctx context.Context, request *pbo.UpdateOrderProduct) (*pbo.OrderProductResponse, error) {
	response, err := o.storage.OrderProduct().Update(ctx, request)
	if err != nil {
		fmt.Println("error in service layer while updating order product by id")
		return nil, err
	}
	return response, nil
}

func (o *orderProductService) Delete(ctx context.Context, request *pbo.GetOrderProductByID) (*pbo.OrderProductResponse, error) {
	response, err := o.storage.OrderProduct().Delete(ctx, request)
	if err != nil {
		fmt.Println("error in service layer while deleting order product by id")
		return nil, err
	}
	return response, nil
}

package service

import (
	"context"
	"fmt"
	"log"
	pbo "order_service/genproto/delivery-protos/order_service"
	pbu "order_service/genproto/delivery-protos/user_service"
	"order_service/grpc/client"
	"order_service/storage"
	"time"
)

type orderService struct {
	storage  storage.IStorage
	services client.IServiceManager
	pbo.UnimplementedOrderServiceServer
}

func NewOrderService(storage storage.IStorage, services client.IServiceManager) *orderService {
	return &orderService{
		storage:  storage,
		services: services,
	}
}

func (o *orderService) Create(ctx context.Context, request *pbo.CreateOrder) (*pbo.Order, error) {

	branchID := request.BranchId

	branch, err := o.services.BranchService().GetByID(context.Background(), &pbu.GetBranchByID{Id: branchID})
	if err != nil {
		log.Println("error while getting branch:", err)
		return nil, err
	}

	client, err := o.services.ClientService().GetByID(context.Background(), &pbu.GetClientByID{Id: request.ClientId})
	if err != nil {
		log.Println("error while getting client:", err)
		return nil, err
	}

	fmt.Println(client.DiscountAmount)
	fmt.Println(request.Discount)

	if (client.DiscountAmount >= request.Discount) && (branch.Id == request.BranchId) {
		fmt.Println(client.DiscountAmount)
		fmt.Println(request.Discount)

		resp, err := o.storage.Order().Create(ctx, request)
		if err != nil {
			log.Println("error in order service in service package while creating order ")
			return nil, err
		}

		return resp, nil
	}
	fmt.Println(err)
	return nil, err

}

func (o *orderService) GetByID(ctx context.Context, request *pbo.GetOrderByID) (*pbo.Order, error) {
	resp, err := o.storage.Order().GetByID(ctx, request)
	if err != nil {
		log.Println("error in service layer while getting order by id")
		return nil, err
	}
	return resp, nil
}

func (o *orderService) GetAll(ctx context.Context, request *pbo.GetOrderList) (*pbo.OrdersResponse, error) {

	orders, err := o.storage.Order().GetAll(ctx, request)

	if err != nil {

		log.Println("error in service layer while getting order list")

		return nil, err

	}
	return orders, nil
}

func (o *orderService) Update(ctx context.Context, request *pbo.UpdateOrder) (*pbo.OrderResponse, error) {
	response, err := o.storage.Order().Update(ctx, request)
	if err != nil {
		log.Println("error in service layer while updating order by id")
		return nil, err
	}
	return response, nil
}

func (o *orderService) Delete(ctx context.Context, request *pbo.GetOrderByID) (*pbo.OrderResponse, error) {
	response, err := o.storage.Order().Delete(ctx, request)
	if err != nil {
		log.Println("error in service layer while deleting order by id")
		return nil, err
	}
	return response, nil
}

func (o *orderService) UpdateOrderStatus(ctx context.Context, request *pbo.Order) (*pbo.OrderResponse, error) {
	response, err := o.storage.Order().UpdateOrderStatus(ctx, request)
	if err != nil {
		log.Println("error in service layer while updating order status order by id")
		return nil, err
	}
	order, err := o.storage.Order().GetByID(ctx, &pbo.GetOrderByID{
		Id: request.Id,
	})
	if err != nil {
		log.Println("error while getting order data")
	}

	if request.Status == "finished" {
		clientID := order.GetClientId()
		if _, err := o.services.ClientService().UpdateFinishedOrder(context.Background(), &pbu.UpdateClientData{
			Id:               clientID,
			LastOrderedDate:  time.Now().String(),
			TotalOrdersSum:   order.GetPrice(),
			TotalOrdersCount: order.GetDiscount(),
			UpdatedAt:        time.Now().String(),
		}); err != nil {
			log.Println("error while updating client data where order status is finished", err.Error())
		}

	}

	return response, nil

}

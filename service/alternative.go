package service

import (
	"context"
	"fmt"
	"log"
	pbo "order_service/genproto/delivery-protos/order_service"
	"order_service/grpc/client"
	"order_service/storage"
)

type alternativeService struct {
	storage  storage.IStorage
	services client.IServiceManager
	pbo.UnimplementedAlternativeServiceServer
}

func NewAlternativeService(storage storage.IStorage, services client.IServiceManager) *alternativeService {
	return &alternativeService{
		storage:  storage,
		services: services,
	}
}

func (a *alternativeService) Create(ctx context.Context, request *pbo.CreateAlternative) (*pbo.Alternative, error) {
	alternative, err := a.storage.Alternative().Create(ctx, request)
	if err != nil {
		log.Println("error in alternative service while creating alternative")
		return nil, err
	}

	return alternative, nil
}

func (a *alternativeService) GetByID(ctx context.Context, request *pbo.GetAlternativeByID) (*pbo.Alternative, error) {
	alternative, err := a.storage.Alternative().GetByID(ctx, request)
	if err != nil {
		fmt.Println("error in service layer while getting alternative by id")
		return nil, err
	}
	return alternative, nil
}

func (a *alternativeService) GetAll(ctx context.Context, request *pbo.GetAlternativeList) (*pbo.AlternativesResponse, error) {
	alternatives, err := a.storage.Alternative().GetAll(ctx, request)
	if err != nil {
		fmt.Println("error in service layer while getting alternative list")
		return nil, err
	}
	return alternatives, nil
}

func (a *alternativeService) Update(ctx context.Context, request *pbo.UpdateAlternative) (*pbo.AlternativeResponse, error) {
	response, err := a.storage.Alternative().Update(ctx, request)
	if err != nil {
		fmt.Println("error in service layer while updating alternative by id")
		return nil, err
	}
	return response, nil
}

func (a *alternativeService) Delete(ctx context.Context, request *pbo.GetAlternativeByID) (*pbo.AlternativeResponse, error) {
	response, err := a.storage.Alternative().Delete(ctx, request)
	if err != nil {
		fmt.Println("error in service layer while deleting alternative by id")
		return nil, err
	}
	return response, nil
}

package service

import (
	"context"
	"fmt"
	"log"
	pbo "order_service/genproto/delivery-protos/order_service"
	"order_service/grpc/client"
	"order_service/storage"
)


type deliveryTariffService struct {
	storage  storage.IStorage
	services client.IServiceManager
    pbo.UnimplementedDeliveryTarifServicesServer
}

func NewDeliveryTariffService(storage storage.IStorage, services client.IServiceManager) *deliveryTariffService {
	return &deliveryTariffService{
		storage:  storage,
		services: services,
	}
}

func (d *deliveryTariffService) Create(ctx context.Context, request *pbo.CreateDeliveryTarif) (*pbo.DeliveryTarif, error) {
	tariff, err := d.storage.DeliveryTariff().Create(ctx, request)
	if err != nil {
		log.Println("error in delivery tariff service while creating delivery tariff")
		return nil, err
	}
	return tariff, nil
}

func (d *deliveryTariffService) GetByID(ctx context.Context, request *pbo.GetDeliveryTarifByID) (*pbo.DeliveryTarif, error) {
	tarif, err := d.storage.DeliveryTariff().GetByID(ctx, request)
	if err != nil {
		fmt.Println("error in service layer while getting delivery tariff by id")
		return nil, err
	}
	return tarif, nil
}

func (d *deliveryTariffService) GetAll(ctx context.Context, request *pbo.GetDeliveryTarifList) (*pbo.DeliveryTarifsResponse, error) {
	tariffs, err := d.storage.DeliveryTariff().GetAll(ctx, request)
	if err != nil {
		fmt.Println("error in service layer while getting delivery tariff list")
		return nil, err
	}
	return tariffs, nil
}

func (d *deliveryTariffService) Update(ctx context.Context, request *pbo.UpdateDeliveryTarif) (*pbo.DeliveryTarifResponse, error) {
	response, err := d.storage.DeliveryTariff().Update(ctx, request)
	if err != nil {
		fmt.Println("error in service layer while updating delivery tariff by id")
		return nil, err
	}
	return response, nil
}

func (d *deliveryTariffService) Delete(ctx context.Context, request *pbo.GetDeliveryTarifByID) (*pbo.DeliveryTarifResponse, error) {
	response, err := d.storage.DeliveryTariff().Delete(ctx, request)
	if err != nil {
		fmt.Println("error in service layer while deleting delivery tariff by id")
		return nil, err
	}
	return response, nil
}

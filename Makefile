
run:
	go run cmd/main.go

swag:
	swag init -g api/router.go -o api/docs

proto:
	find ./genproto -name '*.proto' -exec protoc --go_out=./genproto --go_opt=paths=source_relative {} +
